//START OF MAIN FILE

#include <iostream>
#include "incidence.h"
using namespace std;
 
int main()
{	
	Test Example to Show that Eclipse works
    cout << "The sum of 3 and 5 is " << add(3, 4) << endl;
    return 0;
}

//START OF THE HEADER FILE 
//This is start of the header guard.  ADD_H can be any unique name.  
//By convention, we use the name of the header file.
#ifndef ADD_H
#define ADD_H
 
// This is the content of the .h file, which is where the declarations go
int add(int x, int y); // function prototype for add.h -- don't forget the semicolon!
 
int add(int x, int y)
{
    return x + y;
}

// This is the end of the header guard
#endif