//How to go about creating an Incidence Matrix 
//Should the I/O code be included in the class or just main 
#include <iostream>
#include <fstream>
#include <string>
using namespace std;

int main () {
  int insances;
  ifstream myfile ("example.txt");
  if (myfile.is_open())
  {
    while ( getline (myfile,line) )
    {
      cout << line << '\n';
    }
    myfile.close();
  }

  else cout << "Unable to open file"; 

  return 0;
}