/*
Create a class called Incidencematrix that takes in one arguement, this argument is an integer and determines 
the size of the incidence matrix (int size). This number will be inputted by the user, prompt in main 
could be as follows: "How many rats were used in this experiment?" Then have one function 
(or a constructor) that creates an incidence matrix of size x size with 0's in every location. 
User will be able to specify how many rats were used in this case by creating an object of type Incidencematrix(5) 
for example. Incidencematrix constructor would be passed int size and would take this and create the two dimesional array.
*/ 

//START OF HEADER FILE
#ifndef incidence_H
#define incidence_H
 
#include <iostream>
using namespace std;

class Incidencematrix {
  public:
    Incidencematrix (const int size);
    void display();
};

Incidencematrix::Incidencematrix (const int size) {
	//Do i have to declare s = size; I believe I do
	int s = size;
	Incimat[s][s] = { {0} };
}

void Incidencematrix::display(){
	for(i = 0; i <= s; i++)
		for(j = 0; j <= s; j++)
			cout << Incimat[i][j] << "	" << endl; 
}

// End of header guard 
#endif

//START OF MAIN FILE 
#include <iostream>
#include "incidence.h"
#include <fstream>
using namespace std;

int main () {
  
	  /*int n; 
	  cout << "How many rats were used in your experiment?";
	  cin >> n; then the 3 below would be replaced with n*/
	  
	  Incidencematrix mat1(3);
	  mat1.display();

  return 0;
}

//Ex of working with data files, instead of string we will use int, and will have to add each number to statemat

/*

	//must open file 
	//must read through the file and find how many endl's there are and put that into m 
	//then declare interger 2 dimensional array here, called statemat with n and m
	
  string line;
  ifstream myfile ("example.txt");
  if (myfile.is_open())
  {
    while ( getline (myfile,line) )
    {
      cout << line << '\n';
    }
    myfile.close();
  }

  else cout << "Unable to open file"; 

  return 0;
}*/